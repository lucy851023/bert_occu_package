# -*- coding: utf-8 -*-
"""
Created on Wed Nov  6 18:18:11 2019

@author: Yu-Hsuan Chuang
"""
# sample data from occu_data
import pandas as pd
import os
import numpy as np
from random import sample


path ='D:\\研究所\\lab\\OCCU_2019\\bert_occu_package\\RR2016_A8a_A32a.xlsx'
df = pd.read_excel(path)
arr = np.arange(len(df))
index = sample(arr.tolist(),50)
output_df = df.iloc[index,:]
output_df.to_excel('D:\\研究所\\lab\\OCCU_2019\\bert_occu_package\\sample_data.xlsx',index=0)



