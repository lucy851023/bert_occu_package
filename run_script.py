#!/usr/bin/python
import bert_predict
import argparse

def main():
    parser = argparse.ArgumentParser()
    ## Required parameters
    parser.add_argument("--data_dir", default=None, type=str, required=True,
                        help="The input data dir. Should contain the .tsv files (or other data files) for the task.")
    args = parser.parse_args()
    result = bert_predict.output_result(args.data_dir)
    print(result)
    

if __name__ == "__main__":
    main()
    
